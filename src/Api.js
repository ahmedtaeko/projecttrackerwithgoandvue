import axios from 'axios';

import Auth from './Auth'

export default{

    // post: (endPoint, data = {}) => axios.post(endPoint, data , {
    //     headers: { jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiRXBHZWQ2bXNNaFNWYzRXL00zNFhyQXNoWU1ycnNBMWJnYzZwQU5NPSJ9.AAWCV2X_MNZe8YMMvQgpclOaG4jGzhV_CkLb3dt7YA4"}
    // }),

    // query: (method, endPoint, data) => {

    //     return axios[method](endPoint, data,  {
    //         headers: { jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiRXBHZWQ2bXNNaFNWYzRXL00zNFhyQXNoWU1ycnNBMWJnYzZwQU5NPSJ9.AAWCV2X_MNZe8YMMvQgpclOaG4jGzhV_CkLb3dt7YA4"}
    //     });

    // },

    get: (endPoint) => {

        return axios.get(endPoint, {
            headers: { jwt: Auth.getJWT()}
        
        })


    },
    
    remove: (URL) => {

        return axios.delete(
            URL,
            {headers: {
                jwt: Auth.getJWT()
            }
            },);

    },
    

    query(method, endPoint, data){

        return new Promise( (resolve , reject )=> {

            axios[method](endPoint, data,  {
                headers: { jwt: Auth.getJWT()}
            
            })
            .then(res => {

                resolve(res)
                // this.saveUserId(res.data.data.userId)
            } )
            .catch((res) => {

                reject(res)
                // AuthError
                // commit('AuthError', response.data)
            })

        })

    },



    


}