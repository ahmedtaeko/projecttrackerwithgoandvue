import Api from './Api.js'

export default {

    

    signIn({ username, password }) {
       
        return new Promise( (resolve , reject )=> {
         axios
            .post('/admins/login', {
                username,
                password,
            })
            .then(res =>{
        
                this.saveToken(res.data.jwt)

                resolve(res)

            })
            .catch( err => {

                reject(err)
                // AuthError
                // commit('AuthError', response.data)
            })

        })
    },

    check(){

        return Api.get('/admins/auth/')

        // let auth = false;

        //  Api.get('/admins/auth/').then((response) =>  {

        //     auth = true;
        // })
        // .catch( (error) =>  {

        // })

        // return auth;

        /* if( localStorage.getItem('jwt')){

            return true
        }

        return false */

    },

    loggedIn(){

        return localStorage.getItem('jwt');

    },

    logout(){

        localStorage.clear();

    },

    saveToken(token) {

        localStorage.setItem('jwt', token)
        // user is auth ^_^

    },

    getJWT(){

        return localStorage.getItem('jwt');

    }








}

