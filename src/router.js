import Vue from 'vue'
import Router from 'vue-router'

import Dashboard from './views/Dashboard.vue'

import Login from './views/Login.vue'

import TransactionList from './views/Transaction/List.vue'

import ProjectList from './views/Project/List.vue'
import OrganizationList from './views/Organization/List.vue'
import ItemsList from './views/Item/List.vue'
import ProductsList from './views/Product/List.vue'

import NotFound from "./views/NotFoundPage.vue";


import Auth from './Auth'

Vue.use(Router)

const router =  new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      redirect: "/projects",
      meta: {

        requiresAuth: true

      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta:{

        requiresVisitor: true

      }
    },

    {
      path: '/transactions',
      name: 'transactions',
      component: TransactionList,
      meta: {

        requiresAuth: true

      }
    },
    {
      path: '/projects',
      name: 'projects',
      component: ProjectList,
      meta: {

        requiresAuth: true

      }
    },
    {
      path: '/products',
      name: 'Products',
      component: ProductsList,
      meta: {

        requiresAuth: true

      }
    },
    {
      path: '/items',
      name: 'Items',
      component: ItemsList,
      meta: {

        requiresAuth: true

      }
    },
    {
      path: '/organization',
      name: 'organization',
      component: OrganizationList,
      meta: {

        requiresAuth: true

      }
    },
    { path: "*", component: NotFound },
    


  ]
})


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    Auth.check()
    .then(()=>{
      next()
    })
    .catch(()=>{
      next({
        name: 'login',
      })
    })
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    Auth.check()
    .then(()=>{
      next({
        name: 'projects',
      })
    })
    .catch(()=>{
      next()
    })
  } else {
    next()
  }
})


export default router;