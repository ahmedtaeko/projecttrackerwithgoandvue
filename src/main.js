import Vue from 'vue'
import App from './App.vue'

import vuetify from './plugins/vuetify';
import router from './router'

import _ from 'lodash';

import moment from 'moment';

window._ = _; 

window.moment = moment;

import $ from "jquery";

import axios from 'axios';

import Auth from './Auth'

import Notifications from 'vue-notification'

window.Vue = Vue;

require('./helpers');


import { TableComponent, TableColumn } from 'vue-table-component';

Vue.component('table-component', TableComponent);
Vue.component('table-column', TableColumn);



import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';

Vue.use(Notifications)

window.axios = axios;


window.Auth = Auth;




Vue.config.productionTip = false

window.v = new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
